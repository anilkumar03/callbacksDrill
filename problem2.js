const fs = require('fs')


exports.readFileFunction = (filePath, cb1) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error('Error:', err)
        }
        else {
            cb1(data)
        }

    })
}

exports.writeFileFunction = (filePath, data, cb2) => {
    fs.writeFile(filePath, data, (err, data) => {
        if (err) {
            console.error('Error:', err)
        }
        else {
            cb2(filePath)
        }
    })
}

exports.appendFileFunction = (fileNamesPath, path, cb3) => {
    fs.appendFile(fileNamesPath, path + '/n', (err, data) => {
        if (err) {
            console.error('Error:', err)
        }
        else {
            cb3(path)
        }
    })
}

exports.deleteFilesFunction = (namesData, cb4) => {

    namesData.forEach((path) => {
        if (path.length > 0) {
            fs.unlink(path, (err, data) => {
                if (err) {
                    console.error(err)
                }
            })
        }
    })

    cb4()
}