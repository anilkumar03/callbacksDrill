const path = require('path')

const test = require('../problem1')

const folderPath = 'randomJsonFiles'



test.createFolder(folderPath)

for (let i=1 ;i<=5; i++){
    let filePath = path.resolve(__dirname,'../randomJsonFiles/'+`file${i}.json`)
    test.createFiles(filePath,i)
}

for (let j=1; j<=5; j++){
    let filePath = path.resolve(__dirname,'../randomJsonFiles/'+`file${j}.json`)
    test.deleteFileFunction(filePath)
}

test.deleteFolder(folderPath)