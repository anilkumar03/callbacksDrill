const test = require('../problem2')
const path = require('path')


const dataFilePath = path.resolve(__dirname,'../lipsum.txt')
const fileNamesPath = path.resolve(__dirname,'../fileNames.txt')
const ucFilePath = path.resolve(__dirname,'../uppercaseContent.txt')
const splitedLcFilePath = path.resolve(__dirname,'../splitedLowercase.txt')
const sortedContentFilePath = path.resolve(__dirname,'../sortedContent.txt')

const makecall = ()=>{
    test.readFileFunction(dataFilePath,(lipsumTxt)=>{
        test.writeFileFunction(ucFilePath,lipsumTxt.toUpperCase(),(path1)=>{
            test.appendFileFunction(fileNamesPath,path1,(path2)=>{
                test.readFileFunction(ucFilePath,(ucTxt)=>{
                    test.writeFileFunction(splitedLcFilePath,JSON.stringify(ucTxt.toLowerCase().split('.')),(splitedContent)=>{
                        test.appendFileFunction(fileNamesPath,splitedContent,((path3)=>{
                            test.readFileFunction(splitedLcFilePath,(splitedTxt)=>{
                                test.writeFileFunction(sortedContentFilePath,JSON.parse(splitedTxt).sort(),(sortedContent)=>{
                                    test.appendFileFunction(fileNamesPath,sortedContentFilePath,(path4)=>{
                                        test.readFileFunction(fileNamesPath,(data1)=>{
                                            test.deleteFilesFunction(data1.split('/n'),(data2)=>{
                                                test.writeFileFunction(fileNamesPath,'',(data3)=>{
                                                    console.log('Task completed success')
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        }))
                    })
                })
            })
        })
    })
}

makecall()